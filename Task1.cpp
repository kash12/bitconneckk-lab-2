#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int option;
    float fahrenheit = 0.0;
    double celsius = 0.0;
    float PI = 3.142;
    float radius;
    float area;
    float height;

    do{
    cout << "----------------- MENU --------------------" << endl;
    cout << "1. convert Celsius to Fahrenheit" << endl;
    cout << "2. convert Fahrenheit to Celsius" << endl;
    cout << "3. calculate the circumference of a circle" << endl;
    cout << "4. calculate the area of the Triangle" << endl;
    cout << "5. calculate the area of the Rectangle" << endl;
    cout << "6. calculate the area of the Triangle" << endl;
    cout << "7. calculate the volume of the Cylinder" << endl;
    cout << "8. calculate the volume of the Cone" << endl;
    cout << "9. quit the program" << endl;
    cout << "\nEnter your choice : ";
    cin >> option;

    if(option == 1)
    {
        cout << "\nYou have selected the option of Converting CELSIUS to FAHRENHEIT" << endl;
        cout << "Please enter the temperature in Celsius : ";
        cin >> celsius;
        fahrenheit = (celsius * 9.0) / 5.0 + 32;
        cout << "The temperature in Celsius : " << celsius << endl;
        cout << "The temperature in Fahrenheit : " << fahrenheit << endl;
        cout << endl;
    }

    if(option == 2)
    {
        cout << "\nYou have selected the option of converting Fahrenheit to celsius" << endl;
        cout << "Please enter the temperature in Fahrenheit : ";
        cin >> fahrenheit;
        celsius = (fahrenheit - 32) * 5 / 9;
        cout << "The temperature in Fahrenheit : " << fahrenheit << endl;
        cout << "The temperature in celsius : " << celsius << endl;
        cout << endl;
    }

    if(option == 3)
    {
        double circumference = 0.0;
        radius = 0.0;
        cout << "\nYou have selected the option of calculating circumference of a circle" << endl;
        cout << "Please enter the radius of the circle : ";
        cin >> radius;
        circumference = 2 * PI * radius;
        cout << "The circumference of the circle is " << circumference << endl;
        cout << endl;
    }

    if(option == 4)
    {
        area = 0.0;
        cout << "\nYou have selected the option of calculate area of a circle" << endl;
        cout << "Please enter the radius of the circle : ";
        cin >> radius;
        area = PI * radius * radius;
        cout << "The area of the circle is " << area << endl;
        cout << endl;
    }

    if(option == 5)
    {
        area = 0.0;
        double length = 0.0;
        double width = 0.0;
        cout << "\nYou have selected the option of area of rectangle" << endl;
        cout << "Please enter the length of the rectangle : ";
        cin >> length;
        cout << "Please enter the width of the rectangle : ";
        cin >> width;
        area = length * width;
        cout << "The area of the rectangle is " << area << endl;
        cout << endl;
    }

    if(option == 6)
    {
        float firstSide, secondSide, thirdSide, s, areaOfTriangle;
        cout << "\nYou have selected the option of calculating the area of triangle" << endl;
        cout << "Please enter the length of first side : ";
        cin >> firstSide;
        cout << "Please enter the length of second side : ";
        cin >> secondSide;
        cout << "Please enter the length of third side : ";
        cin >> thirdSide;
        s = (firstSide + secondSide + thirdSide) / 2;
        areaOfTriangle = sqrt(s * (s - firstSide) * (s - secondSide) * (s - thirdSide));
        cout << "The area of the triangle is " << areaOfTriangle << endl;
        cout << endl;
    }

    if(option == 7)
    {
        float volumeOfCylinder = 0.0;
        height = 0.0;
        cout << "\nYou have selected the option of calculating the volume of cylinder" << endl;
        cout << "Please enter the height of the cylinder : ";
        cin >> height;
        cout << "Please enter the radius of the cylinder : ";
        cin >> radius;
        volumeOfCylinder = PI * radius * radius * height;
        cout << "The volume of cylinder is " << volumeOfCylinder << endl;
        cout << endl;
    }

    if(option == 8)
    {
        float volumeOfCone = 0.0;
        radius = 0.0;
        height = 0.0;
        cout << "\nYou have selected the option of calculating the volume of cone" << endl;
        cout << "Please enter the radius of the cone : ";
        cin >> radius;
        cout < "Please enter the height of the cone : ";
        cin >> height;
        volumeOfCone = (1 / 3) * PI * radius * radius * height;
        cout << "The volume of the cone is " << volumeOfCone << endl;
        cout << endl;
    }

    if(option == 9)
    {
        cout << "\nYou have selected the option of quitting the program" << endl;
        return 0;
    }

    }while(option != 0);

    return 0;
}
